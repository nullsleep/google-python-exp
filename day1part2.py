# ** LISTS **

a = [1, 2, 3]
print a
a = [1, 2, 'aaaa']
print a
# By design python have the same syntax across all the data typesself.
# Many of the same operators that work on string work on lists
len(a)

# This does not make a copy. It's a referemce to the object.
b = a
print b
a[0] = 111
print b

# How to make a copy of the List
b = a[:]
a[0] = 666
print "a >>> " + str(a)
print "b >>> " + str(b)

#Lists comparisons
print a == b

# Lists slices
print a[1:3]

# Looping over a List
for x in a:
    print x

# Checking if a value is in a List (or other data type)
print 2 in a
print 0000 in a

# Appending elements from a list
# Appends does not return anything. It modifies the object.
a.append("XXX")
print a

# Removing elements from a list
a.pop(0)
print a

# Deleting objects.
# What 'del' does is that it removes the definition of a from the local scope.
del a

# If we attempt to print a it'll show it is not defined because a was just deleted

a = [1, 2, 3]
del a[1]
print a # The list has now shrinked

# Sorting through the list
# * This is a new method (sorted) of sorting and a better option that using .sort
a = [4, 2, 1, 6]
# It creates a copy of the array sorted. This function knows automatically
# what type is. By looking at the documentation one can feed this function any
# type of iterable data type as well as other parameter such a reverse.
a = sorted(a)
print a
print sorted(a, reverse=True)

a = ['ccc', 'aaaa', 'd', 'bb']
# By default it uses textual sort
print sorted(a)

# Sorted by length
# Original list:  'ccc'  'aaaaa'  'a'   'bb'
# len:             3      4       1     2
# Sorted:         'd'    'bb'    'ccc'  'aaaa'
# Python creates a shadow list of the lengths of each element in the array
print sorted(a, key=len)

# Sorting by the last character
a[1] = 'aaaz'
print a

def last(s):
    return s[-1]

print sorted(a, key=last)

# Concatenating the elements in a list
# 'dot join' creates a string of the elements of a list joined by a given stirng
print ':'.join(a)
print '\n'.join(a)
print 'OIO'.join(a)

# Spliting the elements in a string given a string
# Returns a list of elements that where split by a given string. The string is
# not stored.
b = ':'.join(a)
print b
print b.split(':')

# Spliting a string on white space
xstr = 'to be is to do'
# split() with no argument splits by white space
xarr = xstr.split()
print xarr

# Looping through a list and picking out certain elements
a.append('caaa')
print a
result = []
for s in a:
  if s[0] == 'c':
      result.append(s)
print result

# Counting numbers
# Starts at 0 and goes up to n-1
print range(20)

# ** TUPLES **
# Tuples has a fixed size so it used for a fixed size number of elements.
# Tuples are inmutable.
t = (1, 2, 5)
print t
print len(t)
print t[1]

# List of tuples
a = [(1, "b"), (2, "a"), (1, "a")]
print a
# It sorts by the first element and if they are the same it sort through the
# second one.
print sorted(a)

# To sort by one thing and the sort by another you can make a key function
# that returns a tuple.

# Parallel assigments using tuples
(x, y) = (1, 2)
print x
print y
