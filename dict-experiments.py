# *** DICTIONARY EXPERIMENTS ***

# ** SORTING BY THE VALUES IN A DICTIONARY **
import operator

# By definition, dictionaries are not sorted (to speed up access)
dict = {"Pierre": 42, "Anne": 33, "Zoe": 24}
print "The primary dictionary:"
print dict

# >> Use the sorted function and operator module <<
# To sort the dictionary by values (i.e., the age) one must create another data
# structure such as lit, or an ordered dictionary.
# sorted_dic is a list of tuples sorted by the second element in each tuple.
# Each tuple contains the key and value for each item found in the dictionary.
sorted_dict = sorted(dict.items(), key=operator.itemgetter(1))
print "\nSorting through the dictionary values using the library 'operator':"
print sorted_dict

# >> Use the sorted function and lambda function <<
# Insted of the operator module we can use the lambda function.
# The computation time is of the same order of magnitude as with the operator module.
print "\nSorting through the dictionary values using a lambda function:"
sorted_dict = sorted(dict.items(), key=lambda x: x[1])
print sorted_dict

# Equivalent lambda version:
print "Using an equivalent lambda function:"
sorted_dict = sorted(dict.items(), key=lambda (k, v): v)
print sorted_dict

# >> Using the sorted function from the 'collections' library <<
from collections import OrderedDict
print "\nUsing the sorted function from the 'Collections' library:"
dd = OrderedDict(sorted(dict.items(), key=lambda x: x[1]))
print dd

# >> Using the sorted function and list comprehension <<
print "\nUsing the sorted function and list comprehension:"
sorted_dict = sorted((value, key) for (key, value) in dict.items())
print sorted_dict

# Checking if a value is in the dictionary
print '\n\n'
dx = {'a': 'q', 'b': 'w', 'c': 'e', 'd': 'r'}
if not 'a' in dx:
  print "Value Found"
else:
  print "Value NOT found"

if not 'q' in dx:
  print "Second value Found"
else:
  print "Second value NOT found"
