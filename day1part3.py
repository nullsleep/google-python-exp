# --- PYTHON NOTES ---
# It is very important in python for the variables to indicate the data type.
# This is the only way to know the type explicitly.

# Since python is interpreted rather than compiled it is better to write code
# an inmidiately test it rather than writing a big program only to fail on
# fringe cases that the interepreter will not get. This is because the
# interpreter will only run code at the very last moment once it gets to it.
# Have a goal make the program print whenever you get to that goal and keep iterating
# on it.
# --------------------

def loopingThroughEachLine(filename):
  # w = write - r = reading - rU ignores DOS or UNIX file endings
  f = open(filename, 'rU')

  # Looping through the file contents
  for line in f:
    # Adding a trailing comma at the end inhibits the last new line
    print line,

  # It is more appropiate to close the file manually
  f.close()

def fileContentsArray(filename):
  f = open(filename, 'rU')
  # Getting all the contents of the file and adding them into an array.
  # If the file takes 22GB of data it will require 22GB or RAM
  lines = f.readlines()
  print lines
  f.close()

def fileContentsString(filename):
  f = open(filename, 'rU')
  # Reading the whole file into one String
  text = f.read()
  print text,
  f.close()

# *** DICTIONARIES OR HASH TABLES ***
# Dictionaries are built in key-value data structures. Very quick at key retrival.
# For each key it can find its value very quickly. It is they only thing hastables
# (dictionaries) are fast at. It doesn't matter if it has a million keys.
# The hashing strategy the dictionary uses causes the keys to be in a random order.

d = {}
d['a'] = 'alpha'
d['o'] = 'omega'
d['g'] = 'gamma'

print d
# print d['x'] # Error by looking to a key that doesn't exist
print d.get('x') # Returns none if the value is not there, otherwise it returns the value
print d.get('a')

# Checking if a value is in the dictionary
print 'a' in d
print 'x' in d

# Returning a list of just the keys.
print d.keys()

# Looping through the array by key
for k in sorted(d.keys()):
  print 'Key:', k, '->', d[k]

# Returning a list of values
print d.values()

# Prints the keys and values in a tuple form (key, value)
print d.items()

for tuple in d.items():
  print tuple

# Google really runs as very big hashtable. A hashtable by word.

# *** FILES ***
# In Unix there is an utility function cat that print out the contents of a file.
# These are similar a much more basic implementations.

# This prints out 2 break lines. The default one in the print and the break line.
print "\n"
loopingThroughEachLine("./day1part3-exercises/small.txt")
print "\n"
fileContentsArray("./day1part3-exercises/small.txt")
print "\n"
fileContentsString("./day1part3-exercises/small.txt")
