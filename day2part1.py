# *** REGULAR EXPRESIONS ***
# A way for searching patterns in large texts ala text search in ms word.

import re

# Attributes: pattern, text
# re.search(pat, text)

textToSeach = 'called piiig'

match = re.search('iig', textToSeach)
print match # Returns the object itself
print match.group() # Returns what it found

# Unsuccessful match
match = re.search('igs', textToSeach)
print match
# print match.group() # Retuns AttributeError: 'NoneType' object has no attribute 'group'

def Find(pat, text):
  match = re.search(pat, text)
  if match:
    print match.group()
  else:
    print "Not found"

Find('igs', textToSeach)
Find('ig', textToSeach)

# ** Rules of regular expressions **

# 1. Simple characters match themselves. All of the text has to match.
Find('ed', textToSeach)

# 2. 'dot' matches any character
Find('....g', textToSeach) # Any four characters and a g
Find('x...g', textToSeach) # Not found
Find('..gs', textToSeach) # Not found

# 3. Search is left to right and its going to be satisfied as soon as it finds the first match.
Find('..g', textToSeach + 'aabbg') # It only find the the first g
Find('a..g', textToSeach + 'aabbg') # Now it finds the second g

# Searching for the period character (.)
# Backlash inhibits the special nature of the period and treats it as a character
Find('c\.l', textToSeach + 'c.lled')

# Raw string (r): Do not do any special processing with backlashes
Find(r'c\.l', textToSeach + 'c.lled')

# \w matches a word character
# The difference between dots and \w is that dot can be any character (spaces, characters,
# digits, etc) \w can only be a character (space is not a word character)
print '------'
Find(r':\w\w\w', 'blah :cat blah blah')
print '------'

# \d matches a digit
Find(r'\d\d\d', 'blah :123xxx')

# \s matches whitespaces
Find(r'\d\s\d\s\d', 'blah : 1 2 3')

# +: 1 or more
# *: 0 or more
Find(r'\d\s+\d\s+\d', 'blah : 1    2          3')

Find(r':\w+', 'blah :cool beans man')
# It doesn't find the : because \w only finds word characters not special chracters.
Find(r':\w+', 'blah :cool& beans man')

# Since period can be any character if goes all the way to the end of the line
Find(r':.+', 'blah :cool& beans man')

# \S matches non-whitespace characters
Find(r':\S+', 'blah :cool&beansman123=&asdads kalsjd    asdasd')

# Pulling emails out of text using regular expressions.
# The dot character because it is in square brackets it means literally a dot.
Find(r'[\w.]+@[\w.]+', 'blah car.sands@gmail.com pepe @')
Find(r'\w[\w.]*@[\w.]+', 'blah .car.sands@gmail.com pepe @')

# Picking the user name and the host serparately
# Putting the parenthesis around the parts we cares so we they can be grouped
m = re.search(r'([\w.]+)@([\w.]+)', 'blah .car.sands@gmail.com pepe @')
print m.group()
# Refers to the first set parenthesis
print m.group(1)
# Refers to the second set parenthesis
print m.group(2)

# Takes the pattern and finds all of the patterns
mfa = re.findall(r'[\w.]+@[\w.]+', 'blah .car.sands@gmail.com pepe@gmail')
print mfa

# Returning a list of tuples representing the grouping of each finding
mfax = re.findall(r'([\w.]+)@([\w.]+)', 'blah .car.sands@gmail.com pepe@gmail')
print mfax

# Same regular expression but adding he ignore case parameter
mfaxic = re.findall(r'([\w.]+)@([\w.]+)', 'blah .car.sands@gmail.com pepe@gmail', re.IGNORECASE)
print mfaxic
