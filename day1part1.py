# Created in 1990 by Guido Van Rosum
# Qucik and light language. Open source.
# Scripting language. Similiar to Ruby, Pearl, Javascript
# Good a quick turn around, no compile time.
# Interpreted language.
# Python is case sensitive.

# Python does everything at very last moment possible. Unlike compiled laanguages
# checks very little when it runs and it evaluates each line in the moment, and
# check where the varaibles are pointing, the data types and noticies errors.
# Python only checks a line when it hits that line. Since python doesn't check
# every single line it is important to have full test coverage.

# !/usr/bin/python2.x -tt The flag -tt crahes if there any tabs instead of spaces.
# For python only 2 spaces are recomended for indentation

# sys includes several oeprating system functions. CLI arguments, etc
import sys

def sup(name):
    if name == "Carl" or name == "Jung":
        print "Reactive programming is a declarative programming paradigm concerned with data streams and the propagation of change."
        name = name + "???"
    else:
        print "With this paradigm it is possible to express static (e.g., arrays) or dynamic (e.g., event emitters) data streams with ease, and also communicate that an inferred dependency within the associated execution model exists, which facilitates the automatic propagation of the changed data flow."
    name = name + "!!!!"
    # print 'Sup ' + name
    # The coma adds an sapce automatically unlike the +
    print 'Sup', name

def main():
    print "\n-- THE VERY BASICS --"
    a = 6
    print a
    a = 'Hello'
    print a
    print len(a)
    #print('Hello' + 6) #Fails because the interpreter can't concatenate 2 different types
    print 'Hello' + str(6)

    print "\n-- TESTING THE SYSTEM ARGUMETNS --"
    # Needs to import sys
    print sys.argv # type this in the console: python Day1.py Amor Fati

    # Accessing the documentation. In the python interpreter:
    #dir(sys) # Shows all the symbols defined in sys. Needs to import sys
    #help(sys)
    #help(sys.exit)
    #help(len)

    # Testing the sup function. To test it use somehting like: python Day1.py Carlos
    sup(sys.argv[1])

    print "'Using single quotes'"
    print 'Using double quotes"'

    string1 = "Reactive programming has been proposed as a way to simplify the creation of interactive user interfaces, (near) real time system animation."
    print string1, "yay!"
    # OOP BASICS: <string object> run that method on that object
    # Lower case version of the string. These methods don't modify the original string
    print string1.lower()
    string2 = "YAY"
    print string2.lower()

    # Prints the index of the first character found.
    print string1.find('e')

    print string1[3]
    # print string1[999] # This is an error.

    # This is called a format String
    print 'Hi %s I have %d donuts' % ('Pepe', 666)

    # Python 2.7 Strings are not unicode

    # String slices
    string3 = "Hello"
    print string3[0]
    print string3[1]
    print len(string3)
    print string3[1:3]
    print string3[1:5]
    print string3[1:] # Goes all the way to the end of the String
    print string3[:] # Shows the entire string

    # Varaible string3 = "Hello"
    #  H  e  l  l  o
    #  0  1  2  3  4
    # -5 -4 -3 -2 -1
    print string3[1:3] # Prints el
    print string3[-4:-2] # Prints el
    print string3[:-3] # Prints He
    print string3[-3:] # Prints llo

# Boilerplate code (sintax) to run the main
if __name__ == '__main__':
    main()
